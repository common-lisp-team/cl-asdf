(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "asdf")) ; Force ASDF upgrade

(unless (string= (with-open-file (s #p"version.lisp-expr") (read s))
                 (asdf:asdf-version))
  (format t "ASDF failed to upgrade itself")
  (uiop:quit 1))
